/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensaje_app;

/**
 *
 * @author diego
 */
public class Mensajes {
    
    
    int id_mensajes;
    private String mensaje;
    private String autor_mensaje;
    private String fecha_mensaje;

    
    
    public Mensajes(){
        
    }
    
    public Mensajes(int id_mensaje, String mensaje, String autor_mensaje, String fecha_mensaje) {
       this.id_mensajes =id_mensaje;
        this.mensaje = mensaje;
        this.autor_mensaje = autor_mensaje;
        this.fecha_mensaje = fecha_mensaje;
    }

    public int getId_mensajes() {
        return id_mensajes;
    }

    public void setId_mensajes(int id_mensajes) {
        this.id_mensajes = id_mensajes;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * @return the autor_mensaje
     */
    public String getAutor_mensaje() {
        return autor_mensaje;
    }

    /**
     * @param autor_mensaje the autor_mensaje to set
     */
    public void setAutor_mensaje(String autor_mensaje) {
        this.autor_mensaje = autor_mensaje;
    }

    /**
     * @return the fecha_mensaje
     */
    public String getFecha_mensaje() {
        return fecha_mensaje;
    }

    /**
     * @param fecha_mensaje the fecha_mensaje to set
     */
    public void setFecha_mensaje(String fecha_mensaje) {
        this.fecha_mensaje = fecha_mensaje;
    }
    
    
     
}


