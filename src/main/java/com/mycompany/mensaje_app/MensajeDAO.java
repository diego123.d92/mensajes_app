/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensaje_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author diego
 */
public class MensajeDAO {
    
    
    public static void CrearMensajeDB(Mensajes mensaje){
       Conexion db_conexion = new Conexion();
       
        try (Connection conexion = db_conexion.get_Connection()){
            
            PreparedStatement ps = null;
            
            try {
                String query = "INSERT INTO mensajes(mensaje,autor_mensaje)VALUES(?,?)";
                ps =conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("mensaje Guardado");
 
            } catch (Exception e) {
                System.out.println("mensaje no enviado hay un eroror en el sistema ");
            }

        } catch (Exception e) {
            System.out.println("error"+e);
        }
       
    } 
    
    
    public static void ListaMensajes(){
     
        Conexion conexion_bd = new Conexion();
        
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        try (Connection conexion = conexion_bd.get_Connection()){
          String query = "SELECT *FROM mensajes";
          ps = conexion.prepareStatement(query);
          rs = ps.executeQuery();
          
          while(rs.next()){
              System.out.println("-----------------------------------------");
              System.out.println("ID: "+rs.getInt("id_mensaje"));
              System.out.println("Mensaje: "+rs.getString("mensaje"));
              System.out.println("Autor: "+ rs.getString("autor_mensaje"));
              System.out.println("Fecha: "+rs.getString("fechamensaje"));
          }
        } catch (Exception e) {
            System.out.println("erro"+e);
        }
        
    }
    
    
    public static  void EliminarMensaje(int id_mensaje){
        Conexion conexionbd = new Conexion();
        
        try(Connection conexion =conexionbd.get_Connection()) {
            PreparedStatement ps = null;
            try{
                    
            String query ="DELETE FROM mensajes WHERE id_mensaje =?";
            ps= conexion.prepareStatement(query);
            ps.setInt(1, id_mensaje);
            ps.executeUpdate();
            System.out.println("el mensaje a sido Borrado");
            
            }catch(SQLException e){
                System.out.println("Error "+e);
            }
            
            
        } catch (Exception e) {
        }
        
        
    }

    public static void AcualizartDatos(Mensajes mensaje){
        Conexion conexionbd = new Conexion();
        
        try (Connection conexion = conexionbd.get_Connection()){
            PreparedStatement ps = null;
            
            try {
                String query ="UPDATE mensajes SET mensaje=? WHERE id_mensaje=?";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2,mensaje.getId_mensajes());
                ps.executeUpdate();
                System.out.println("Mensaje Actualizado");
            } catch (Exception e) {
                System.out.println("error"+e);
                
            }
        } catch (Exception e) {
             System.out.println("error"+e);
        }
        
        
    }
    
}
